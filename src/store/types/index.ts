export * from './common';

export type actionType = {
	type: string,
	payload: any
};
