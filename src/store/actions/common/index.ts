import { SetIsAuthLoadingType } from '@store/types';
import { common } from '@store/actionTypes';
import { waiter } from '@src/utils/Functions';

export const setIsLoading = async ( bool: boolean ): Promise<SetIsAuthLoadingType> => {
	await waiter( 2000 );
	return {
		type: common.SET_IS_AUTH_LOADING,
		payload: bool,
	};
};

export const triggerDarkMode = ( bool: boolean ): SetIsAuthLoadingType => ( {
	type: common.TRIGGER_THEME_DARK_MODE,
	payload: bool,
} );
