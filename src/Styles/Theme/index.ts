import { ThemeType } from './types';
import { common, darkTheme, lightTheme } from './colors';

export const getTheme = ( isDark: boolean ): ThemeType => {
	if ( isDark ) {
		return {
			...common,
			...darkTheme,
		};
	} else return {
		...common,
		...lightTheme,
	};
};
