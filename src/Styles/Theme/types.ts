export type CommonStylesType = {
	RED_COLOR: string
}

export type ModeStylesType = {
	PRIMARY_COLOR: string
}

export type ThemeType = CommonStylesType & ModeStylesType;
