import { CommonStylesType, ModeStylesType } from './types';

export const common: CommonStylesType = {
	RED_COLOR: '#F30828',
};

export const lightTheme: ModeStylesType = {
	PRIMARY_COLOR: '#3798F3',
};

export const darkTheme: ModeStylesType = {
	PRIMARY_COLOR: '#1f304e',
};
