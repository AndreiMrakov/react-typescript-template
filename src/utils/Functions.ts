export const waiter = ( time: number ) => new Promise( ( res ) => setTimeout( res, time ) );
