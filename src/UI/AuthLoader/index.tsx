import React from 'react';
import styled from 'styled-components';
import { ThemeType } from '@src/Styles/Theme/types';

type PropsType = {
	duration?: number,
	size?: number,
	color?: string,
	theme?: ThemeType | object
}

const AuthLoader =
	styled.div<PropsType>`
		animation: rotate ${ ( { duration } ): number => duration || 1 }s ease-in-out infinite;
		width:  ${ ( { size } ) => size || 100 }px;
		height:  ${ ( { size } ) => size || 100 }px;
		border-radius: 50%;
		border-style: solid;
    	border-width: 10px;
    	margin: 0 auto;
		border-color: transparent ${ ( { color, theme } ): string => `${ ( color || theme?.PRIMARY_COLOR ) } `.repeat( 2 ) };
		
		@keyframes rotate {
			from {
				transform: rotate(0);
			}
			to {
				transform: rotate(360deg);
			}
		}
	`;
export default AuthLoader;
