import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ReducerType } from '@store/reducers';
import { ThemeProvider } from 'styled-components';
import { setIsLoading } from '@store/actions/common';

import AuthLoader from '@src/UI/AuthLoader';
import { getTheme } from '@src/Styles/Theme';
import image from '@images/papey-gavna-original.jpg'

import styles from './styles.module.scss';

const Application: React.FC = () => {
	const { loading, isDarkMode } = useSelector( ( state: ReducerType ) => state.commonReducer );
	const dispatch = useDispatch();

	useEffect( () => {
		const setIsLoadingFalse = async (): Promise<void> => {
			dispatch( await setIsLoading( false ) );
		};
		setIsLoadingFalse();
	}, [] );
	return (
		<ThemeProvider theme = { getTheme( isDarkMode ) }>
			<div className = { styles.container }>
				{
					loading
						? <AuthLoader />
						: <img src = { image } alt = "popey" style={{maxHeight: '50vh'}}/>
				}
			</div>
		</ThemeProvider>
	);
};

export default Application;
