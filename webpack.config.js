const path = require( 'path' );
const autoprefixer = require( 'autoprefixer' );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );
const { CleanWebpackPlugin } = require( 'clean-webpack-plugin' );
const ErrorOverlayPlugin = require( 'error-overlay-webpack-plugin' );
const DotEnv = require( 'dotenv-webpack' );
const { CheckerPlugin } = require( 'awesome-typescript-loader' );

module.exports = ( env, argv ) => {
	const isDevelopment = argv.mode === 'development';
	return {
		entry: `./src/index.tsx`,
		devServer: {
			historyApiFallback: true,
			hot: true,
			headers: {
				'Access-Control-Allow-Origin': '*',
			},
			// open: true,
		},
		resolve: {
			alias: {
				//DUPLICATE ALIASES TO TSCONFIG!!!
				'@src': path.resolve( __dirname, 'src' ),
				'@images': path.resolve( __dirname, 'src/assets/images' ),
				'@store': path.resolve( __dirname, 'src/store' ),
			},
			extensions: [ '.js', '.jsx', '.ts', '.tsx', '.scss' ],
		},
		devtool: 'cheap-module-source-map',
		output: {
			path: path.join( __dirname, '/build' ),
			filename: isDevelopment ? `[name].js` : `[name]---[hash].js`,
			publicPath: '/',
		},
		module: {
			rules: [
				{
					test: /\.js[x]?$/,
					exclude: /node_modules/,
					use: [ 'babel-loader' ],
				},
				{
					test: /\.tsx?$/,
					exclude: /node_modules/,
					use: [ 'awesome-typescript-loader' ],
				},
				{
					test: /\.css$/,
					exclude: /\.module\.css$/,
					use: [
						'css-hot-loader',
						isDevelopment
							? 'style-loader'
							: MiniCssExtractPlugin.loader,
						'css-loader',
						{
							loader: 'postcss-loader',
							options: {
								plugins: [ autoprefixer ],
								sourceMap: true,
							},
						},
					],
				},
				{
					test: /\.(sa|sc)ss$/,
					exclude: /\.module\.(sa|sc)ss$/,
					use: [
						'css-hot-loader',
						isDevelopment
							? 'style-loader'
							: MiniCssExtractPlugin.loader,
						{
							loader: 'css-loader',
							options: {
								sourceMap: isDevelopment,
							},
						},
						{
							loader: 'postcss-loader',
							options: {
								plugins: [ autoprefixer ],
								sourceMap: true,
							},
						},
						{
							loader: 'sass-loader',
							options: {
								sourceMap: isDevelopment,
							},
						},
					],
				},
				{
					test: /\.module\.scss$/,
					use: [
						'css-hot-loader',
						isDevelopment
							? 'style-loader'
							: MiniCssExtractPlugin.loader,
						// 'css-modules-typescript-loader',
						{
							loader: 'css-loader',
							options: {
								modules: {
									localIdentName: isDevelopment
										? '[local]__[hash:base64:5]'
										: '[hash:base64:16]',
								},
								sourceMap: isDevelopment,
							},
						},
						{
							loader: 'postcss-loader',
							options: {
								plugins: [ autoprefixer ],
								sourceMap: true,
							},
						},
						{
							loader: 'sass-loader',
							options: {
								sourceMap: isDevelopment,
							},
						},
					],
				},

				{
					test: /\.(jp(e*)g|png|gif)$/,
					include: [
						path.join( __dirname, 'src/assets/images/' ),
					],
					use: [
						{
							loader: 'file-loader',
							options: {
								name: isDevelopment
									? '[name].[ext]'
									: '[name].[ext]?[hash]',
								outputPath: 'images/',
							},
						},
					],
				},
				{
					test: /.*\.svg$/,
					include: [
						path.join( __dirname, 'src/assets/images/' ),
					],
					use: [
						{
							loader: 'file-loader',
							options: {
								name: isDevelopment
									? '[name].[ext]'
									: '[name].[ext]?[hash]',
								outputPath: 'images/',
							},
						},
					],
				},
				// {
				// 	test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
				// 	include: [
				// 		path.join( __dirname, 'src/assets/fonts/' ),
				// 	],
				// 	use: [
				// {
				// 	loader: 'file-loader',
				// 	options: {
				// 		name: '[name].[ext]',
				// 		outputPath: 'assets/fonts/',
				// 		// name: 'assets/images/[path][name].[ext]?[hash]',
				// 		publicPath: './src',
				// 		useRelativePath: true,
				// 		context: './src',
				// 	},
				// },
				// ],
				// },
			],
		},
		plugins: [
			new CleanWebpackPlugin( {
				cleanOnceBeforeBuildPatterns: [ '**/*', '!static-files*' ],
			} ),
			new HtmlWebpackPlugin( {
				template: 'src/public/template.html',
				favicon: 'src/public/favicon.ico',
				filename: 'index.html',
				title: 'Social Network',
				hash: !isDevelopment,
			} ),
			new MiniCssExtractPlugin( {
				filename: isDevelopment ? '[name].css' : '[name]---[hash].css',
				chunkFilename: isDevelopment ? '[id].css' : '[id].[hash].css',
			} ),
			new CheckerPlugin(),
			new ErrorOverlayPlugin(),
			new DotEnv(),
		],
	};
};
